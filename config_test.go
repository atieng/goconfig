package config_test

import (
	"testing"
	"time"

	. "action-target.net/scs2ctrl/config"
)

func TestEverything(t *testing.T) {
	Bool("auger/foo_dot_bar/enabled", false, "bool value")
	Bool("test_bool", false, "bool value")
	Int("test_int", 0xdeadbeef, "int value")
	Int64("test_int64", 0, "int64 value")
	Uint("test_uint", 0, "uint value")
	Uint64("test_uint64", 0, "uint64 value")
	String("test_string", "0", "string value")
	Float64("test_float64", 0.123456789, "float64 value")
	Duration("test_duration", 10*time.Second, "time.Duration value")

	SetFile("conftest.conf")
	Load()
	Print()
	//Save()
}
